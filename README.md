Challenges
==========

This repo contains the source of CTF-type challenges, written in JavaScript for
training/fun/whatever. Some need access to the source to solve, others just 
need a brain.

Happy Hacking!

- Rich
