const Monitor = require('forever-monitor').Monitor;
const challenges = require('./challenges.json');

const children = {};

challenges.forEach(x => {
    const child = new Monitor(
        x.path,
        Object.assign(
            {
                silent: true,
                watch: false,
                minUptime: 1
            },
            x.config
        )
    );
    child.on('stdout', data => {
        data.toString('utf8')
            .split('\n')
            .forEach(line => {
                if (line.trim()) console.log(`${x.title}:  ${line.trim()}`);
            });
    });
    child.start();
    children[x.title] = child;
});
