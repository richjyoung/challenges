FROM node:10.6-alpine

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

WORKDIR /usr/src/app
COPY package.json .
RUN npm install

EXPOSE 1337-1339
COPY . .

CMD ["npm", "start"]
